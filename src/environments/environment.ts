// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mapbox: {
    accessToken: 'pk.eyJ1IjoiYWxleGtobCIsImEiOiJja2EyOTIzY2cwODhxM2RvY2R0dXo4NDY1In0.iOfVnKvX6Xjfsw5jDsq2vg'
  },
  userDataSource: 'http://localhost:8000/chp/user/data',
  userLogin: 'http://localhost:8000/chp/user/login',
  carsListDataSource: 'http://localhost:8000/chp/cars-list',
  carHistoryDataSource: 'http://localhost:8000/chp/car-history',
  autoCalculation: 'http://localhost:8000/chp/auto-calculation',
  carInfo: 'http://localhost:8000/chp/car-info',
  storeLocations: 'http://localhost:8000/chp/store-locations',
  sendOrderData: 'http://localhost:8000/orders/order-data-send',
  getServiceTypes: 'http://localhost:8000/orders/services-types-list',
  promoDataSource: 'http://localhost:8000/promo/promo-list',
  getPromotion: 'http://localhost:8000/promo/get-promo'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
