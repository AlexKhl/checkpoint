import {Component, Input, OnInit} from '@angular/core';
import {MenuController, Platform} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {AuthenticationService} from '../../services/authentication.service';
import {ModalComponent} from '../../components/modal/modal.component';
import {ModalCallComponent} from '../../components/modal-call/modal-call.component';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {ModalPromotionComponent} from '../../components/modal-promotion/modal-promotion.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @Input() promoId;
  private promoDataSource: string = environment.promoDataSource;
  promoList = [];

  constructor(
    public menuCtrl: MenuController,
    private _translate: TranslateService,
    public auth: AuthenticationService,
    public modal: ModalComponent,
    public modalCall: ModalCallComponent,
    public modalPromo: ModalPromotionComponent,
    private http: HttpClient,
    private platform: Platform,
  ) {
    this.http.get(this.promoDataSource).subscribe(
      data => {
        // tslint:disable-next-line:forin
        for (const ind in data) {
          this.promoList.push(data[ind]);
        }
        console.log(data);
        this.promoList = this.promoList.reduce(function (result, value, index, array) {
          if (index % 2 === 0) {
            result.push(array.slice(index, index + 2));
          }
          return result;
        }, []);
        console.log(this.promoList);
      },
      error => {
        console.log(error);
      }
    );

    // Back button for exit from app
    // this.platform.backButton.subscribe(() => {
    //   navigator['app'].exitApp();
    // });
  }

  openPromo(id): void {
    this.modalPromo.presentPromoModal(id);
  }

  // https://stackoverflow.com/questions/53803975/how-to-toggle-menu-in-ionic-4
  showNotifications() {
    this.menuCtrl.toggle('right-notif-menu');
  }

  openOrder(): void {
    this.modal.presentOrderModal();
  }

  openCall(): void {
    this.modalCall.presentCallModal();
  }

  ngOnInit(): void {
  }
}
