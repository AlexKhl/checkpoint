import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {MenuController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {ModalComponent} from '../../../components/modal/modal.component';
import {ModalCallComponent} from '../../../components/modal-call/modal-call.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  // alexkh
  // 71588891

  username = null;
  password = null;

  constructor(
    public auth: AuthenticationService,
    public menuCtrl: MenuController,
    public modal: ModalComponent,
    public modalCall: ModalCallComponent,
    private translate: TranslateService
  ) {
  }

  ngOnInit() {
  }

  showNotifications() {
    this.menuCtrl.toggle('right-notif-menu');
  }

  openOrder(): void {
    this.modal.presentOrderModal();
  }

  openCall(): void {
    this.modalCall.presentCallModal();
  }

  login() {
    this.auth.login(this.username, this.password);
  }

}
