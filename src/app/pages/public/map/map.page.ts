import { Component, OnInit } from '@angular/core';
import {MenuController, ModalController} from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import * as mapboxgl from 'mapbox-gl';
import {environment} from '../../../../environments/environment';
import {ModalComponent} from '../../../components/modal/modal.component';
import {ModalCallComponent} from '../../../components/modal-call/modal-call.component';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  latitude: any;
  longitude: any;
  map: mapboxgl.Map;
  style: 'mapbox://styles/mapbox/outdoors-v9';
  // style: 'mapbox://styles/mapbox/streets-v11';

  constructor(
    private modalCtrl: ModalController,
    private geolocation: Geolocation,
    public menuCtrl: MenuController,
    public modal: ModalComponent,
    public modalCall: ModalCallComponent,
    private translate: TranslateService
  ) {
    mapboxgl.accessToken = environment.mapbox.accessToken;
    this.getCurrentCoordinates();
  }

  getCurrentCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }

  ionViewDidEnter() {
    this.buildMap();
    new mapboxgl.Marker({ 'color': '#113483' }).setLngLat([74.62700301890493, 42.886069194546536]).setPopup(new mapboxgl.Popup().setHTML('<h4>' + this.translate.instant('CheckPoint Жибек-Жолу') + '</h4>')).addTo(this.map);
    new mapboxgl.Marker({ 'color': '#113483' }).setLngLat([74.56106194690003, 42.84349018222338]).setPopup(new mapboxgl.Popup().setHTML('<h4>CheckPoint Джал</h4>')).addTo(this.map);
    new mapboxgl.Marker({ 'color': '#f9df13' }).setLngLat([74.54354275210736, 42.83745726140708]).setPopup(new mapboxgl.Popup().setHTML('<h4>CheckPoint Чортекова</h4>')).addTo(this.map);
    new mapboxgl.Marker({ 'color': '#f9df13' }).setLngLat([74.62722487251202, 42.87118568469688]).setPopup(new mapboxgl.Popup().setHTML('<h4>CheckPoint Восток-5</h4>')).addTo(this.map);
  }

  ngOnInit() {
    // this.getCurrentCoordinates();
    // this.buildMap();
  }

  buildMap() {
    this.map = new mapboxgl.Map({
      container: 'map', // container id
      style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
      center: [74.58983229599045, 42.86215262796756], // starting position [lng, lat]
      zoom: 11 // starting zoom
    });
    console.log(this.longitude);
    console.log(this.latitude);
  }

  openOrder(): void {
    this.modal.presentOrderModal();
  }

  openCall(): void {
    this.modalCall.presentCallModal();
  }

  showNotifications() {
    this.menuCtrl.toggle('right-notif-menu');
  }
}
