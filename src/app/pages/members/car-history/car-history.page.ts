import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {KeyValue, Location} from '@angular/common';
import {AlertController, MenuController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {ModalComponent} from '../../../components/modal/modal.component';
import { IonContent } from '@ionic/angular';
import {ModalCalculatorComponent} from '../../../components/modal-calculator/modal-calculator.component';

@Component({
  selector: 'app-car-history',
  templateUrl: './car-history.page.html',
  styleUrls: ['./car-history.page.scss'],
})
export class CarHistoryPage implements OnInit {
  @ViewChild(IonContent) ionContent: IonContent;
  private carHistoryDataSource: string = environment.carHistoryDataSource;
  private carInfo: string = environment.carInfo;
  cid = null;
  carInfoData: Object = [];
  groupByName = [];
  mileage = {};

  // tslint:disable-next-line:max-line-length
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private storage: Storage,
    private http: HttpClient,
    private _location: Location,
    public menuCtrl: MenuController,
    private _translate: TranslateService,
    public auth: AuthenticationService,
    public modal: ModalComponent,
    public modalCalculator: ModalCalculatorComponent,
    public alertController: AlertController,
    private translate: TranslateService
  ) {
  }

  ngOnInit() {
    // this.cid = this.route.snapshot.queryParamMap.get('cid');
    this.route.queryParamMap.subscribe(queryParams => {
      this.cid = queryParams.get('cid');
      this.storage.get('auth-token').then(token => {
        const formData: FormData = new FormData();
        formData.append('token', token);
        formData.append('cid', this.cid);

        const formDataCar: FormData = new FormData();
        formDataCar.append('cid', this.cid);
        this.http.post(this.carInfo, formDataCar).subscribe(
          data => {
            this.carInfoData = data;
          }
        );

        this.http.post(this.carHistoryDataSource, formData).subscribe(
          data => {
            const history_data = data;
            // tslint:disable-next-line:forin
            for (const i in history_data) {

              this.groupByName[data[i].date] = this.groupByName [data[i].date] || [];
              this.groupByName[data[i].date]['product_name'] = this.groupByName [data[i].date]['product_name'] || [];
              this.groupByName[data[i].date]['hdbk_title'] = this.groupByName [data[i].date]['hdbk_title'] || [];
              this.groupByName[data[i].date]['mileage'] = this.groupByName [data[i].date]['mileage'] || [];
              this.groupByName[data[i].date]['mileage_interval'] = this.groupByName [data[i].date]['mileage_interval'] || [];
              this.groupByName[data[i].date]['date'] = data[i].date;

              if (!this.groupByName[data[i].date]['product_name'].includes(data[i].product_name)) {
                this.groupByName [data[i].date]['product_name'].push(data[i].product_name);
              }
              if (!this.groupByName[data[i].date]['hdbk_title'].includes(data[i].hdbk_title)) {
                this.groupByName [data[i].date]['hdbk_title'].push(data[i].hdbk_title);
              }
              if (!this.groupByName[data[i].date]['mileage'].includes(data[i].mileage)) {
                this.groupByName [data[i].date]['mileage'].push(data[i].mileage);
              }
              if (!this.groupByName[data[i].date]['mileage_interval'].includes(data[i].mileage_interval)) {
                this.groupByName [data[i].date]['mileage_interval'].push(data[i].mileage_interval);
              }
            }
          },
          error => {
            console.log(error);
          }
        );
      });
    });
  }

  async presentAlertCheckbox() {
    const alert = await this.alertController.create({
      inputs: [
        {
          name: 'mileage',
          type: 'text',
          label: 'Введите текущий пробег',
        }
      ],
      header: this.translate.instant('TYPE CURRENT MILEAGE'),
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            this.mileage['mileage'] = data['mileage'];
            this.mileage['cid'] = this.cid;
            this.openCalculator(this.mileage);
          }
        }
      ]
    });

    await alert.present();
  }

  openOrder(): void {
    this.modal.presentOrderModal();
  }

  showNotifications() {
    this.menuCtrl.toggle('right-notif-menu');
  }

  backClicked() {
    this._location.back();
  }

  openCalculator(car): void {
    this.ionContent.scrollToTop(500);
    this.modalCalculator.presentCalcModal(car);
  }

  originalOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return 0;
  }
  reverseKeyOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return a.key > b.key ? -1 : (b.key > a.key ? 1 : 0);
  }
  valueOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return a.value.localeCompare(b.value);
  }
}
