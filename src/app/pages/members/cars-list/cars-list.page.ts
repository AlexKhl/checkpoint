import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserDataService} from '../../../services/user-data.service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../services/authentication.service';
import {Storage} from '@ionic/storage';
import {environment} from '../../../../environments/environment';
import {MenuController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {ModalComponent} from '../../../components/modal/modal.component';
import {ModalCallComponent} from '../../../components/modal-call/modal-call.component';

@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.page.html',
  styleUrls: ['./cars-list.page.scss'],
})
export class CarsListPage implements OnInit {
  private carsListDataSource: string = environment.carsListDataSource;
  carList = [];

  // tslint:disable-next-line:max-line-length
  constructor(
    private http: HttpClient,
    private data: UserDataService,
    private router: Router,
    private auth: AuthenticationService,
    private storage: Storage,
    public menuCtrl: MenuController,
    private _translate: TranslateService,
    public modal: ModalComponent,
    public modalCall: ModalCallComponent,
  ) {
    this.storage.get('auth-token').then(token => {
      const formData: FormData = new FormData();
      formData.append('token', token);
      this.http.post(this.carsListDataSource, formData).subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        data => {
          // tslint:disable-next-line:forin
          for (const car in data) {
            this.carList.push(data[car]);
          }
        },
        error => {
          console.log(error);
        }
      );
    });
  }

  openOrder(): void {
    this.modal.presentOrderModal();
  }

  openCall(): void {
    this.modalCall.presentCallModal();
  }

  ngOnInit() {
  }

  showNotifications() {
    this.menuCtrl.toggle('right-notif-menu');
  }

  openCarServiceHistory(cid) {
    // console.log(cid);
    this.router.navigate(['/car'], {queryParams: {cid: cid}});
  }

}
