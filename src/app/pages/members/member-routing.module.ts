import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'logout', loadChildren: () => import('src/app/pages/members/logout/logout.module').then(m => m.LogoutPageModule) },
  { path: 'cars-list', loadChildren: () => import('src/app/pages/members/cars-list/cars-list.module').then(m => m.CarsListPageModule) },
  // tslint:disable-next-line:max-line-length
  { path: 'car-history', loadChildren: () => import('src/app/pages/members/car-history/car-history.module').then(m => m.CarHistoryPageModule) },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MemberRoutingModule { }
