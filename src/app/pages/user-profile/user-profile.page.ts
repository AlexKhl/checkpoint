import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment';
import {UserDataService} from '../../services/user-data.service';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {MenuController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {AuthenticationService} from '../../services/authentication.service';
import {ModalComponent} from '../../components/modal/modal.component';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {

  private userDataSource: string = environment.userDataSource;

  private userName: string;
  private secondName: string;
  private patronym: string;
  private nickName: string;
  private phone: string;

  // tslint:disable-next-line:max-line-length
  constructor(
    private http: HttpClient,
    private data: UserDataService,
    private router: Router,
    private storage: Storage,
    public menuCtrl: MenuController,
    private _translate: TranslateService,
    public auth: AuthenticationService,
    public modal: ModalComponent,
  ) {
    this.storage.get('auth-token').then(token => {
      const formData: FormData = new FormData();
      formData.append('token', token);
      this.http.post(this.userDataSource, formData).subscribe(
          // tslint:disable-next-line:no-shadowed-variable
        data => {
          this.nickName = data['login'];
          this.userName = data['name'];
          this.secondName = data['surname'];
          this.patronym = data['patronym'];
          this.phone = data['phone'];

          console.log(data);
        },
        error => {
          console.log(error);
        }
      );
    });
  }

  openOrder(): void {
    this.modal.presentOrderModal();
  }

  ngOnInit() {
  }

  showNotifications() {
    this.menuCtrl.toggle('right-notif-menu');
  }

}
