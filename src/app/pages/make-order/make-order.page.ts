import {Component, Input, OnInit} from '@angular/core';
import {LoadingController, MenuController, ModalController, AlertController } from '@ionic/angular';
import {FormBuilder} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {LoadingComponent} from '../../components/loading/loading.component';
import {ToastComponent} from '../../components/toast/toast.component';
import {AuthenticationService} from '../../services/authentication.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-make-order',
  templateUrl: './make-order.page.html',
  styleUrls: ['./make-order.page.scss'],
})

export class MakeOrderPage implements OnInit {
  private sendOrderData: string = environment.sendOrderData;
  private getServiceTypesList: string = environment.getServiceTypes;
  // Data passed in by componentProps
  @Input() stores: Object;
  @Input() cars: Object;

  orderForm;
  time;
  servicesList;
  checkedServices;
  checkboxHeader;

  constructor(
    public menuCtrl: MenuController,
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private httpClient: HttpClient,
    private loading: LoadingComponent,
    private toast: ToastComponent,
    private authService: AuthenticationService,
    public alertController: AlertController,
    private translate: TranslateService
  ) {
    // Define checked service items variable as an array
    this.checkedServices = [];
    // Set date for time field
    this.time = new Date();
    this.time.setHours(this.time.getHours() + 1);
    this.time.setMinutes(this.time.getMinutes());
    // Form set
    this.orderForm = this.formBuilder.group({
      serviceDate: new Date().toISOString(),
      serviceStore: '',
      serviceTime: this.time.toISOString(),
      servicedCar: '',
      serviceTypes: '',
      comments: '',
      carBrand: '',
      carModel: '',
      carNumber: '',
      gearBox: '',
      driveType: '',
      fuelType: '',
      engineVolume: '',
      carYear: '',
      userPhone: '',
    });
  }

  ngOnInit(): void {}

  showNotifications() {
    this.menuCtrl.toggle('right-notif-menu');
  }

  sendOrder(formValues) {
    const formDataOrder: FormData = new FormData();
    formDataOrder.append('service_date', formValues.serviceDate);
    formDataOrder.append('service_store_id', formValues.serviceStore);
    formDataOrder.append('service_time', formValues.serviceTime);
    formDataOrder.append('serviced_car_id', formValues.servicedCar);
    formDataOrder.append('serviced_car', formValues.servicedCar);
    formDataOrder.append('comments', formValues.comments);
    formDataOrder.append('car_brand', formValues.carBrand);
    formDataOrder.append('car_model', formValues.carModel);
    formDataOrder.append('car_number', formValues.carNumber);
    formDataOrder.append('gear_box', formValues.gearBox);
    formDataOrder.append('drive_type', formValues.driveType);
    formDataOrder.append('fuel_type', formValues.fuelType);
    formDataOrder.append('service_types', this.checkedServices);
    formDataOrder.append('engine_volume', formValues.engineVolume);
    formDataOrder.append('car_year', formValues.carYear);
    formDataOrder.append('user_phone', formValues.userPhone);

    this.httpClient.post(this.sendOrderData, formDataOrder).toPromise().then(
      data => {
        console.log(data);
        this.modalCtrl.dismiss({
          'dismissed': true
        });
      }
    ).then(() => {
      setTimeout(() => {
        this.toast.presentToast(this.translate.instant('THE ORDER HAS JUST SENT'), 2000);
      }, 1000);
    });
  }

  async presentAlertCheckbox() {
    await this.httpClient.get(this.getServiceTypesList).toPromise().then(
      data => {
        const chckdSrvs = this.checkedServices;
        this.servicesList = Object.keys(data).map(function(serviceInd) {
          const service = {
            name: data[serviceInd]['machine_name'],
            type: 'checkbox',
            value: data[serviceInd]['id'],
            label: data[serviceInd]['name'],
            checked: (chckdSrvs.includes(data[serviceInd]['id']))
          };
          return service;
      });
      }
    );

    // https://ionicframework.com/docs/api/alert
    const alert = await this.alertController.create({
      inputs: this.servicesList,
      header: this.translate.instant('CHOOSE NEEDED SERVICES'),
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            this.checkedServices = data;
          }
        }
      ]
    });

    await alert.present();
  }

  goBack() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
