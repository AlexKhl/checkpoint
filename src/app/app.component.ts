import { Component } from '@angular/core';

import {MenuController, Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {Router} from '@angular/router';
import {AuthenticationService} from './services/authentication.service';
import {NotificationsListComponent} from './pages/members/notifications-list/notifications-list.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})

export class AppComponent {
  NotificationsList: NotificationsListComponent;

  public appPages = [
    {
      title: 'HOME',
      url: '/home',
      icon: 'home',
      anon: true,
      signed: true,
    },
    {
      title: 'PROFILE',
      url: '/user-profile',
      icon: 'person',
      anon: false,
      signed: true,
    },
    {
      title: 'CARS LIST',
      url: '/cars-list',
      icon: 'car',
      anon: false,
      signed: true,
    },
    {
      title: 'ADDRESSES',
      url: '/addresses',
      icon: 'map',
      anon: true,
      signed: true,
    },
    {
      title: 'LOGIN',
      url: '/login',
      icon: 'log-in',
      anon: true,
      signed: false,
    },
    {
      title: 'LOGOUT',
      url: '/logout',
      icon: 'log-out',
      anon: false,
      signed: true,
    }
  ];

  public authState;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private authenticationService: AuthenticationService,
    public menuCtrl: MenuController,
    private _translate: TranslateService
  ) {
    this.initializeApp();
    this.platform
     .ready()
     .then(() => {
        this._initTranslate();
     });
    _translate.setDefaultLang('ru');
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.authenticationService.authenticationState.subscribe(state => {
        if (state) {
          this.authState = true;
          this.router.navigate(['home']);
        } else {
          this.authState = false;
          this.router.navigate(['home']);
        }
      });
    });
  }

  hideNotifications() {
    this.menuCtrl.close('right-notif-menu');
  }

  hideMenu() {
    this.menuCtrl.close('left-user-menu');
  }

  private _initTranslate() {
     // Set the default language for translation strings, and the current language.
     this._translate.setDefaultLang('en');


     if (this._translate.getBrowserLang() !== undefined) {
         this._translate.use(this._translate.getBrowserLang());
     } else {
         this._translate.use('en'); // Set your language here
     }
  }
}
