import {NgModule, PipeTransform} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {IonicStorageModule} from '@ionic/storage';
import {AuthenticationService} from './services/authentication.service';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {AuthGuard} from './guards/auth.guard';
import {NotificationsListComponent} from './pages/members/notifications-list/notifications-list.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ModalComponent} from './components/modal/modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {LoadingComponent} from './components/loading/loading.component';
import {ToastComponent} from './components/toast/toast.component';
import DateTimeFormat = Intl.DateTimeFormat;
import {ModalCallComponent} from './components/modal-call/modal-call.component';
import {CallNumber} from '@ionic-native/call-number/ngx';
import { Vibration } from '@ionic-native/vibration/ngx';
import {ModalPromotionComponent} from './components/modal-promotion/modal-promotion.component';
import {ModalCalculatorComponent} from './components/modal-calculator/modal-calculator.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';



// https://masteringionic.com/blog/2018-07-14-creating-a-multi-language-ionic-translation-app-with-ngx-translate/
// https://ionicframework.com/docs/v3/developer-resources/ng2-translate/
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    NotificationsListComponent,
    ModalComponent,
    ModalCallComponent,
    ModalPromotionComponent,
    ModalCalculatorComponent,
    LoadingComponent,
    ToastComponent,
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    ReactiveFormsModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthenticationService,
    AuthGuard,
    ModalComponent,
    ModalCallComponent,
    ModalPromotionComponent,
    ModalCalculatorComponent,
    CallNumber,
    Vibration,
    LoadingComponent,
    ToastComponent,
    DateTimeFormat,
    Geolocation,
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
