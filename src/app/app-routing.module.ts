import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('src/app/pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'user-profile',
    canActivate: [AuthGuard],
    loadChildren: () => import('src/app/pages/user-profile/user-profile.module').then(m => m.UserProfilePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('src/app/pages/public/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('src/app/pages/members/logout/logout.module').then(m => m.LogoutPageModule)
  },
  {
    path: 'cars-list',
    loadChildren: () => import('src/app/pages/members/cars-list/cars-list.module').then(m => m.CarsListPageModule)
  },
  {
    path: 'car',
    loadChildren: () => import('src/app/pages/members/car-history/car-history.module').then(m => m.CarHistoryPageModule)
  },
  {
    path: 'members',
    loadChildren: () => import('src/app/pages/members/member-routing.module').then(m => m.MemberRoutingModule)
  },
  {
    path: 'register',
    loadChildren: () => import('src/app/pages/public/register/register.module').then(m => m.RegisterPageModule)
  },
  {
    path: 'make-order',
    loadChildren: () => import('src/app/pages/make-order/make-order.module').then(m => m.MakeOrderPageModule)
  },
  {
    path: 'addresses',
    loadChildren: () => import('src/app/pages/public/map/map.module').then( m => m.MapPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
