import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Platform} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';

const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private userLogin: string = environment.userLogin;

  authenticationState = new BehaviorSubject(false);
  public currentUser: Observable<any>;

  constructor(private storage: Storage, private plt: Platform, private http: HttpClient, private router: Router) {
    this.authenticationState = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.authenticationState.asObservable();

    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    });
  }

  login(username, password) {

    // return this.http.post<any>('chp/user/login', { username, password })
    //         .pipe(map(user => {
    //             console.log(user);
    //             // store user details and jwt token in local storage to keep user logged in between page refreshes
    //             localStorage.setItem('currentUser', JSON.stringify(user));
    //             this.authenticationState.next(user);
    //             return user;
    //         }));
    const formData: FormData = new FormData();
    formData.append('username', username);
    formData.append('password', password);
    this.http.post(this.userLogin, formData).subscribe(
      data => {
        this.storage.set(TOKEN_KEY, data['token']).then(() => {
          this.authenticationState.next(true);
        });
        this.router.navigate(['user-profile']);
      },
      error => {
        // this.openDialog();
        console.log(error);
      }
    );

    // return this.storage.set(TOKEN_KEY, 'Bearer 1234567').then(() => {
    //   this.authenticationState.next(true);
    // });
  }

  logout() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
    });
  }

  isAuthenticated() {
    return this.authenticationState.value;
  }
}
