import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  private userData = new BehaviorSubject([]);
  user = this.userData.asObservable();

  constructor() { }

  setPin(user: []) {
    this.userData.next(user);
  }
}
