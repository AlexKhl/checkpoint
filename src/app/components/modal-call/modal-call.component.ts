import { Component } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {CallNumber} from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-modal-call',
  templateUrl: './modal-call.component.html',
  styleUrls: ['./modal-call.component.scss'],
})
export class ModalCallComponent {

  number = '';
  buttonClass = 'deactivated';

  constructor(
    public modalController: ModalController,
    private callNumber: CallNumber,
    ) { }

  async presentCallModal() {
    // @ts-ignore
    const modal = await this.modalController.create({
      component: ModalCallComponent
    });
    return await modal.present();
  }

  goBack() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  doCall(number) {
    this.callNumber.callNumber(number, false)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  setNumber(num) {
    this.number = num;
    if (num) {
      this.buttonClass = 'activated';
    } else {
      this.buttonClass = 'deactivated';
    }
  }
}
