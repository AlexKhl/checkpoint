import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalComponent } from '../modal/modal.component';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-modal-promotion',
  templateUrl: './modal-promotion.component.html',
  styleUrls: ['./modal-promotion.component.scss'],
})
export class ModalPromotionComponent {
  promotion;
  private getPromotion: string = environment.getPromotion;

  constructor(
    public modalController: ModalController,
    public modalOrder: ModalComponent,
    private httpClient: HttpClient,
  ) {  }

  async presentPromoModal(id) {
    const formData: FormData = new FormData();
    formData.append('id', id);
    await this.httpClient.post(this.getPromotion, formData).toPromise().then(
      data => {
        this.promotion = data[0];
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );

    const modal = await this.modalController.create({
      component: ModalPromotionComponent,
      componentProps: {
        'promotion': this.promotion
      }
    });
    return await modal.present();
  }

  goBack() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  openOrder(): void {
    this.modalOrder.presentOrderModal();
  }
}
