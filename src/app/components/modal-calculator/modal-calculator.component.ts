import { Component } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ModalComponent} from '../modal/modal.component';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-modal-calculator',
  templateUrl: './modal-calculator.component.html',
  styleUrls: ['./modal-calculator.component.scss'],
})
export class ModalCalculatorComponent {
  calculation = [];
  private autoCalculation: string = environment.autoCalculation;
  cid;

  constructor(
    private modalController: ModalController,
    private modalOrder: ModalComponent,
    private http: HttpClient,
  ) { }

  async presentCalcModal(car) {
    this.calculation = [];
    console.log('-= presentCalcModal =-');
    console.log(car);
    const formDataCalc: FormData = new FormData();
    formDataCalc.append('cid', car['cid']);
    formDataCalc.append('mileage', car['mileage']);
    await this.http.post(this.autoCalculation, formDataCalc).subscribe(
      data => {
        // tslint:disable-next-line:forin
        for (const ind in data) {
          data[ind]['difference'] = Number(data[ind]['difference']);
          this.calculation.push(data[ind]);
        }
        console.log('- = calculator modal = -');
        console.log(car);
        console.log(this.calculation);
      },
      error => {
        console.log(error);
      }
    );

    const modal = await this.modalController.create({
      component: ModalCalculatorComponent,
      componentProps: {
        'calculation': this.calculation
      }
    });
    return await modal.present();
  }

  goBack() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  openOrder(): void {
    this.modalOrder.presentOrderModal();
  }
}
