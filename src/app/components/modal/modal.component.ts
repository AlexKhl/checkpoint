import { Component } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {MakeOrderPage} from '../../pages/make-order/make-order.page';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {AuthenticationService} from '../../services/authentication.service';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent {
  private storeLocations: string = environment.storeLocations;
  private userCars: string = environment.carsListDataSource;
  private mapProps = [];
  private carsList = [];
  private token;

  constructor(
    public modalController: ModalController,
    private httpClient: HttpClient,
    private auth: AuthenticationService,
    private storage: Storage,
  ) { }

  async presentOrderModal() {
    await this.httpClient.get(this.storeLocations).toPromise().then(data => {
      this.mapProps = Object.keys(data).map(function(storeInd) {
          const store = data[storeInd];
          return store;
      });
    });

    await this.storage.get('auth-token').then(token => {
      this.token = token;
    });

    if (this.auth.isAuthenticated()) {
      const formData: FormData = new FormData();
      formData.append('token', this.token);
      await this.httpClient.post(this.userCars, formData).toPromise().then(
          data => {
            this.carsList = Object.keys(data).map(function(carInd) {
              const car = data[carInd];
              return car;
            });
          },
          error => {
            console.log(error);
          }
        );
    } else {
      this.carsList = [];
    }

    const modal = await this.modalController.create({
      component: MakeOrderPage,
      componentProps: {
        'stores': this.mapProps,
        'cars': this.carsList
      }
    });
    return await modal.present();
  }

}
