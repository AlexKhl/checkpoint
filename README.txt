For running app on mobile:
ionic cordova run android

For running in browser:
"ionic serve" or "ng serve"

For running in emulator:
cordova run --target=Nexus_5_API_28 android --emulator

Run Django:
python manage.py runserver


## USEFUL

Ionic custom colors creation
https://medium.com/@paulstelzer/ionic-4-how-to-add-more-colors-and-use-them-as-color-in-buttons-and-more-7175ab4ae4e7

Create ionic components

ionic generate
ionic generate page
ionic generate page pages/contact
ionic generate component components/form
ionic generate component login-form --change-detection=OnPush
ionic generate directive ripple --skip-import
ionic generate service services/utility

Notifications
https://ionicframework.com/docs/native/local-notifications


App link for editing
https://play.google.com/apps/publish/?account=9144385649262477443#MarketListingPlace:p=tmp.05405522981076679031.1588601696697.1996016699&appid=4975524227051277301


Commands for using cordova:

ionic cordova platform remove android - removes the virtual device
ionic cordova platform add android - adds virtual device
cordova run android --device  - run app on real device
cordova run android --emulator  - run app on emulator
adb logcat  - logs information on emulator
cordova clean android  - cleans the android app
ionic cordova resources - build logo, and other resourses for Android and iOS


Java for Cordova
https://stackoverflow.com/questions/22307516/gradle-finds-wrong-java-home-even-though-its-correctly-set/22656646#22656646


If: Your JAVA_HOME is invalid: /usr/lib/jvm/java-8-oracle
Then: sudo ln -s /usr/lib/jvm/java-7-oracle /usr/lib/jvm/default-java
https://askubuntu.com/questions/1171037/how-to-set-java-path-to-jdk-8
