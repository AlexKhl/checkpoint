from django.contrib.auth.models import User
from rest_framework import serializers
from .models import ChpToken


class ChpTokenSerializer(serializers.HyperlinkedModelSerializer):

    def create(self, validated_data):
        return ChpToken.objects.create(**validated_data)

    class Meta:
        model = ChpToken
        fields = ['token', 'chp_user_id', 'created_date']
