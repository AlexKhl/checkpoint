from django.db import models

class ChpToken(models.Model):
    token = models.CharField(max_length=255, unique=True)
    chp_user_id = models.IntegerField(null=False)
    created_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.token

    class Meta:
        managed = True
        db_table = 'chp_token'
