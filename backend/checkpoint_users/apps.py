from django.apps import AppConfig


class CheckpointUsersConfig(AppConfig):
    name = 'checkpoint_users'
