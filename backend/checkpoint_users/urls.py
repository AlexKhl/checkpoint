from django.urls import path
from .views import ObtainAuthToken, GetProfileData, GetCarsList, GetCarHistory, GetCarInfo, GetStores, AutoCalculation
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    path('user/data', GetProfileData.as_view()),
    path('cars-list', GetCarsList.as_view()),
    path('car-history', GetCarHistory.as_view()),
    path('car-info', GetCarInfo.as_view()),
    path('store-locations', GetStores.as_view()),
    path('auto-calculation', AutoCalculation.as_view()),
    path('user/login', ObtainAuthToken.as_view()),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
]
