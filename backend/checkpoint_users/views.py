from django.http import Http404, JsonResponse
from django.db import connections
from django.utils import timezone
from rest_framework import views
from rest_framework.authtoken.models import Token
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import ChpTokenSerializer
from .models import ChpToken
import bcrypt

# https://simpleisbetterthancomplex.com/tutorial/2018/11/22/how-to-implement-token-authentication-using-django-rest-framework.html
# $2y$10$QiJku/ENmSc9UjvdZ4PBw.lYB1FRW6BcEi91vbEHg0mYhslNJIVfm for 71588891 string
# bcrypt.checkpw('71588891'.encode('UTF-8'), '$2y$10$QiJku/ENmSc9UjvdZ4PBw.lYB1FRW6BcEi91vbEHg0mYhslNJIVfm'.encode('UTF-8'))


def verify_extra_user_token_id(username, password):
    with connections["checkpoint"].cursor() as cursor:
        cursor.execute("SELECT * FROM users WHERE login = %s", [username])
        row = cursor.fetchone()
    return bcrypt.checkpw(password.encode('UTF-8'), row[7].encode('UTF-8'))


def get_user(username):
    with connections["checkpoint"].cursor() as cursor:
        cursor.execute("SELECT * FROM users WHERE login = %s", [username])

        desc = cursor.description
        res = [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]
        return res[0]


def get_user_by_token(ext_id):
    with connections["checkpoint"].cursor() as cursor:
        cursor.execute("SELECT * FROM users WHERE id = %s", [ext_id])
        # cursor.execute("SELECT * FROM users LEFT JOIN it_users ON users.id_1c = it_users.cp_user WHERE users.id = %s", [ext_id])
        # row = cursor.fetchone()
        desc = cursor.description
        res = [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

        return res[0]


def get_cars_list(uid):
    with connections["checkpoint"].cursor() as cursor:
        cursor.execute("SELECT * FROM cars WHERE owner_id = %s", [uid])
        desc = cursor.description
        res = [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]
        return res


class GetProfileData(RetrieveAPIView):
    serializer_class = ChpTokenSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        users = ChpToken.objects.all().filter(token=data['token'])
        serializer = ChpTokenSerializer(users, many=True)

        user = get_user_by_token(serializer.data[0]['chp_user_id'])
        del user['id_1c']
        del user['password']
        del user['remember_token']

        return Response(user)

class ObtainAuthToken(APIView):

    def post(self, request, *args, **kwargs):
        data = request.data
        res_user = {}

        if verify_extra_user_token_id(data["username"], data["password"]):
            user = get_user(data["username"])

            if user:
                token = Token.generate_key(self)
                chp_token_inst = ChpToken.objects.create(chp_user_id=user['id'], token=token, created_date=timezone.now())
                res_user['login'] = user['login']
                res_user['name'] = user['name']
                res_user['token'] = token
                res_user['surname'] = user['surname']

                return Response(res_user)

        return JsonResponse({'status': 'false', 'error': 'Bad Credentials, check the Access Token and/or the UID'},
                            status=403)


class GetCarsList(RetrieveAPIView):
    def post(self, request, *args, **kwargs):
        data = request.data
        users = ChpToken.objects.all().filter(token=data['token'])
        serializer = ChpTokenSerializer(users, many=True)

        user = get_user_by_token(serializer.data[0]['chp_user_id'])
        cars_list = get_cars_list(user['id_1c'])

        return Response(cars_list)


class GetCarInfo(RetrieveAPIView):
    def post(self, request, *args, **kwargs):
        data = request.data
        cid = data['cid']

        with connections["checkpoint"].cursor() as cursor:
            cursor.execute("SELECT name, model, engine_capacity, engine_type, transmission_type, drive_type, `year`, `number` FROM cars WHERE id_1c = %s LIMIT 1", [cid])
            desc = cursor.description
            res = [
                dict(zip([col[0] for col in desc], row))
                for row in cursor.fetchall()
            ]
            return Response(res[0])


class GetCarHistory(RetrieveAPIView):
    def post(self, request, *args, **kwargs):
        data = request.data
        cid = data['cid']

        with connections["checkpoint"].cursor() as cursor:
            # cursor.execute("SELECT car_service.date, car_service.mileage, car_service.mileage_interval, hdbk_services.title AS hdbk_title, services.title AS service_title, hdbk_products.title AS product_name FROM car_service LEFT JOIN services ON car_service.service_id = services.id_1c LEFT JOIN hdbk_services ON car_service.service_id = hdbk_services.id_1c LEFT JOIN user_product ON car_service.id_1c = user_product.id_1c LEFT JOIN hdbk_products ON user_product.product_id = hdbk_products.id_1c WHERE    car_service.car_id = %s AND car_service.car_id IS NOT NULL ORDER BY car_service.date ASC", [cid])

            cursor.execute("SELECT car_service.date, car_service.mileage, car_service.mileage_interval, COALESCE(checkpoint.services.title, checkpoint.hdbk_services.title) AS hdbk_title, hdbk_products.title AS product_name FROM car_service LEFT JOIN services ON car_service.service_id = services.id_1c LEFT JOIN hdbk_services ON car_service.service_id = hdbk_services.id_1c LEFT JOIN user_product ON car_service.id_1c = user_product.id_1c LEFT JOIN hdbk_products ON user_product.product_id = hdbk_products.id_1c WHERE car_service.car_id = %s AND car_service.car_id IS NOT NULL ORDER BY car_service.date ASC", [cid])

            desc = cursor.description
            res = [
                dict(zip([col[0] for col in desc], row))
                for row in cursor.fetchall()
            ]
            return Response(res)


class GetStores(RetrieveAPIView):
    def get(self, request, *args, **kwargs):

        with connections["checkpoint"].cursor() as cursor:
            cursor.execute(
                "SELECT id, title, address, phone, opening_hours, preview, lat, lng, zoom FROM outlets WHERE active=1")
            desc = cursor.description
            res = [
                dict(zip([col[0] for col in desc], row))
                for row in cursor.fetchall()
            ]
            return Response(res)


class AutoCalculation(RetrieveAPIView):
    def post(self, request, *args, **kwargs):
        cid = request.data['cid']
        current_mileage = request.data['mileage']
        with connections["checkpoint"].cursor() as cursor:
            # cursor.execute("SELECT * FROM car_service LEFT JOIN services ON car_service.service_id = services.id_1c LEFT JOIN hdbk_services ON car_service.service_id = hdbk_services.id_1c WHERE car_id = %s AND `date` IN (SELECT MAX(`date`) FROM car_service WHERE car_id = %s GROUP BY service_id)", [cid, cid])
            cursor.execute("SELECT car_service.id, car_service.date, car_service.mileage, car_service.mileage_interval, coalesce(services.title, hdbk_services.title) AS title, (mileage+mileage_interval - %s) AS difference FROM checkpoint.car_service LEFT JOIN checkpoint.services ON car_service.service_id = services.id_1c LEFT JOIN checkpoint.hdbk_services ON car_service.service_id = hdbk_services.id_1c WHERE car_id = %s AND `date` IN (SELECT max(date) FROM checkpoint.car_service WHERE car_id = %s GROUP BY service_id);", [current_mileage, cid, cid]);
            desc = cursor.description
            res = [
                dict(zip([col[0] for col in desc], row))
                for row in cursor.fetchall()
            ]
            return Response(res)
