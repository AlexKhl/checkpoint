from django.urls import path
from .views import AllPromotionsData, SinglePromo

urlpatterns = [
    path('promo-list', AllPromotionsData.as_view()),
    path('get-promo', SinglePromo.as_view())
]
