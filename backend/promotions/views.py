import MySQLdb
from django.http import Http404, JsonResponse
from django.db import connections
from rest_framework.views import APIView
from rest_framework.response import Response


def _get_all_promotions():
    with connections["checkpoint"].cursor() as cursor:
        cursor.execute("SELECT `id`, `title`, `content`, `image`, `active`, `on_main`, `date`, `created_at` FROM news WHERE `active` = 1 AND `image` IS NOT NULL ORDER BY `created_at` DESC LIMIT 2")
        desc = cursor.description
        res = [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]
        for ind, item in enumerate(res):
            if item['image']:
                res[ind]['image'] = 'http://www.checkpoint.kg/uploads/news/full/' + item['image']
        return res


def _get_single_promotion(pid):
    with connections["checkpoint"].cursor() as cursor:
        cursor.execute("SELECT `id`, `title`, `content`, `image`, `date`, `created_at` FROM news WHERE `id` = %s LIMIT 1", [pid])
        res = cursor.fetchone()
    return res


class AllPromotionsData(APIView):
    def get(self, request):
        data = _get_all_promotions()
        return Response(data)


class SinglePromo(APIView):
    def post(self, request):
        promo = _get_single_promotion(request.data['id'])
        return Response({promo});
