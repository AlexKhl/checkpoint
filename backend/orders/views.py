from collections import namedtuple

from rest_framework import status
from rest_framework.generics import RetrieveAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import OrderSerializer, ServiceTypeSerializer
from django.db import connections
from .models import ServiceType


def get_car_info(cid):
    with connections["checkpoint"].cursor() as cursor:
        cursor.execute(
            "SELECT `name`, `model`, `engine_capacity`, `engine_type`, `transmission_type`, `drive_type`, `year`, `number` FROM cars WHERE id = %s LIMIT 1",
            [cid])
        desc = cursor.description
        res = [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]
        return res[0]


def _get_services_list(ids):
    with connections["default"].cursor() as cursor:
        # cursor.execute("SELECT `name` FROM `chp_service_types` WHERE `id` IN (%s)", [ids])
        cursor.execute("SELECT `name` FROM `chp_service_types` WHERE FIND_IN_SET(`id`, %s)", [ids])
        return [i[0] for i in cursor.fetchall()]



def _get_text_from_list(list):
    services = ''
    for service in list:
        services += service + ', '
    return services


class CRUDOrderData(APIView):

    def post(self, request, format=None):
        car_info = {}
        if request.data['serviced_car_id'] == '0' or request.data['serviced_car_id'] == '':
            car_info['name'] = request.data['car_brand']
            car_info['model'] = request.data['car_model']
            car_info['engine_capacity'] = request.data['engine_volume']
            car_info['engine_type'] = request.data['fuel_type']
            car_info['transmission_type'] = request.data['gear_box']
            car_info['year'] = request.data['car_year']
            car_info['number'] = request.data['car_number']
            car_info['drive_type'] = request.data['drive_type']
        else:
            car_info = get_car_info(self.request.data['serviced_car'])

        serviced_car = 'Марка: ' + car_info['name'] + '; Модель: ' + car_info['model'] + '; Объём двигателя: ' + str(
            car_info['engine_capacity']) + '; Тип топлива: ' + car_info['engine_type'] + '; Тип трансмиссии: ' + \
                       car_info['transmission_type'] + '; Тип привода: ' + car_info[
                           'drive_type'] + '; Год выпуска: ' + str(car_info['year']) + '; Номер: ' + car_info[
                           'number'] + ': Список услуг: ' + _get_text_from_list(_get_services_list(request.data['service_types']))

        serializer = OrderSerializer(data=request.data)
        if serializer.is_valid(self):
            serializer.validated_data['serviced_car'] = serviced_car
            if request.data['serviced_car_id'] == '0':
                serializer.validated_data['serviced_car_id'] = None

            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CRUDServiceTypeData(APIView):

    def post(self, request):
        serializer = ServiceTypeSerializer(data=request.data)
        serializer.save()
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)

    def get(self, request):
        services = ServiceType.objects.all()
        serializer = ServiceTypeSerializer(services, many=True)
        return Response(serializer.data)
