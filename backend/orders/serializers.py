from rest_framework import serializers
from .models import Order, ServiceType


class OrderSerializer(serializers.Serializer):

    class Meta:
        model = Order
        fields = ('service_date', 'service_time', 'service_store_id', 'serviced_car_id', 'comments', 'serviced_car',)


    service_date = serializers.DateField(input_formats=['%Y-%m-%dT%H:%M:%S.%fZ'])
    service_time = serializers.DateTimeField(input_formats=['%Y-%m-%dT%H:%M:%S.%fZ'])
    service_store_id = serializers.IntegerField()
    serviced_car_id = serializers.IntegerField(required=False)
    serviced_car = serializers.CharField(allow_blank=True)
    comments = serializers.CharField(required=False)

    def create(self, validated_data):
        order = Order(**validated_data)
        order.save()
        return order

    def update(self, instance, validated_data):
        instance.service_date = validated_data.get('service_date', instance.service_date)
        instance.service_time = validated_data.get('service_time', instance.service_time)
        instance.service_store_id = validated_data.get('service_store_id', instance.service_store_id)
        instance.serviced_car_id = validated_data.get('serviced_car_id', instance.serviced_car_id)
        instance.serviced_car = validated_data.get('serviced_car', instance.serviced_car)
        instance.comments = validated_data.get('comments', instance.comments)
        instance.save()
        return instance


class ServiceTypeSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    name = serializers.CharField(required=True)
    machine_name = serializers.CharField(required=True)
    class Meta:
        model = ServiceType
        fields = ['id', 'name', 'machine_name']

    def create(self, validated_data):
        service_type = ServiceType(**validated_data)
        service_type.save()
        return service_type

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.machine_name = validated_data.get('machine_name', instance.machine_name)
        instance.save()
        return instance
