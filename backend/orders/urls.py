from django.urls import path
from .views import CRUDOrderData, CRUDServiceTypeData
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    path('order-data-send', CRUDOrderData.as_view()),
    path('services-types-list', CRUDServiceTypeData.as_view())
]
