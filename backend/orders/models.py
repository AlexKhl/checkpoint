from django.db import models


class Order(models.Model):
    id = models.AutoField(primary_key=True)
    service_date = models.DateTimeField(blank=False, null=False)
    service_store_id = models.IntegerField(null=False, default=None)
    service_time = models.TimeField(blank=False, null=False)
    serviced_car_id = models.IntegerField(null=True, default=None, blank=True)
    serviced_car = models.TextField(max_length=512, default=None, blank=True)
    comments = models.TextField(max_length=512, null=True, blank=True)

    def __str__(self):
        return "Car system number: " + str(self.serviced_car_id)

    class Meta:
        managed = True
        db_table = 'chp_orders'



class ServiceType(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=128, default=None, null=False)
    machine_name = models.CharField(max_length=128, default=None, null=False)

    def __str__(self):
        return self.name

    class Meta:
        managed = True
        db_table = 'chp_service_types'
