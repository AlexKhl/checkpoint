from django.contrib import admin
from .models import ServiceType, Order

class ServiceTypeAdmin(admin.ModelAdmin):
    fields = ['name', 'machine_name']


class OrderAdmin(admin.ModelAdmin):
    model = Order
    # list_display = ['service_date', 'service_time', 'serviced_car_id', ]
    # list_editable = ['service_date', 'service_time', 'serviced_car_id', ]
    fields = ['service_date', 'service_time', 'serviced_car_id']


admin.site.register(ServiceType)
admin.site.register(Order)
